package main

import (
	"net/http"
	"strconv"

	"echo_s/mq"

	"github.com/labstack/echo"
)

func main() {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		msg := c.QueryParam("msg")
		for a := 0; a < 100; a++ {
			go mq.SendMsgFromRabbit(`{"ucid":"say","msg":"` + strconv.Itoa(a) + "_" + msg + `"}`)
		}
		return c.String(http.StatusOK, msg)
	})
	e.Start("localhost:1323")
	// e.Run(standard.New(":1323"))
}

func init() {
	mq.SendMsgFromRabbit(`{"ucid":"hello","msg":"Hello,World!"}`)
}
